# Scraping API with JWT

A REST API to scrape images.
JWT based authentication.

## Stack

* Python 3
* Flask for REST server app
* PyJWT for JWT
* passlib for password hashing
* PostgreSQL (uses sqlite in DEV mode and Postgres in PROD and for tests)
* requests, BeautifulSoap4
* Flask-Testing extension and requests mock for testing

## Setup and Configuration

### Init repository and virtualenv
This step requires Python 3 installed and the utility pyvenv

```
$ git clone https://nappodo@bitbucket.org/nappodo/flask_jwt.git
$ cd flask_jwt
$ pyvenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

### Configuration and DB setup
You can setup some environment variables, mainly for Postgres SQL 
**Note: check that it doesn't overwrite any of your existing variables**

* CONFIGURATION_MODE  # defaults 'DEV'; choices are 'DEV', 'PROD', 'TEST'
* DB_NAME  # defaults 'jwt_evonove'
* DB_USER  # defaults 'postgres'
* DB_PASS  # defaults '' *(empty string)*
* DB_IP  # defaults 'localhost'
* SECRET_KEY  # defaults to random bytes

Then create two empty databases on PostgreSQL, named ```DB_NAME``` and ```DB_NAME```_test for the user ```DB_USER```
One is for production and the other will be used by the test suite.

Example:

```
$ createdb -U postgres -E utf-8 -O postgres jwt_evonove -T template0
$ createdb -U postgres -E utf-8 -O postgres jwt_evonove_test -T template0
```

### Run tests

```
$ python tests/run_all.py  # run test suite
$ python tests/test_user_api.py  # single test
```

### Run server
This command will run the Flask development server in DEV mode. It will create a sqlite file named ```DB_NAME```.sqlite, if not existing yet.
If you want to use PostgreSQL, set ```CONFIGURATION_MODE``` to 'PROD' or 'TEST'.

```
$ export CONFIGURATION_MODE=PROD
$ python main.py
```

### Dev notes

* Service endpoints mapping and views functions are under ```server.routes``` package
* Protected views are decorated with ```server.decorators.jwt_required```
* Scraping code is in ```server.scraping``` and User model is defined in ```server.models```
* There is a Postman configuration (```flask_jwt_postman.json```) to import in Postman Google Chrome extension, 
with some test calls already configured for dev server.
