# Scraping API with JWT

A REST API to scrape images.
JWT based authentication.

## API Docs
Note: For calls that are "JWT required", you need to add the header ```Authorization``` with value 'Bearer <jwt_token>', 
otherwise you'll receive authentication errors.
### Users API

#### Create a new user
POST http://localhost:5000/api/users/

*Input JSON:*
```
{
  "name": "John",
  "email": "email@test.com",
  "password": "password"
}
```

*Response JSON:*
```
{
  "auth_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE0OTI5NTg0NDEsImV4cCI6MTQ5Mjk1OTA0MSwic3ViIjoyfQ.Bnd7mB0mMpUiSpfnUTJOYBtxg3LKLc5C6WXAmQ3G0fU",
  "message": "Successfully Registered"
}
```

#### Login an existing user
Used to retrieve the auth token for an existing user

POST http://localhost:5000/api/users/signin/

*Input JSON:*
```
{
  "email": "email@test.com",
  "password": "password"
}
```

*Response JSON:*
```
{
  "auth_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0OTI5NjI2NDIsImlhdCI6MTQ5Mjk2MjA0Miwic3ViIjoxfQ.tqD5FKrgozk215OGEmFsjpdjI49XnFvHkHviSJKZTos",
  "message": "Successfully Logged in"
}
```


#### Get registered users

**JWT authentication required**

Return a list of names of registered users

GET http://localhost:5000/api/users/


*Response JSON:*
```
{
  "users": [
    {
      "name": "John",
      "registered_on": "Sun, 23 Apr 2017 16:39:35 GMT"
    },
    {
      "name": "Janet",
      "registered_on": "Sun, 23 Apr 2017 16:40:40 GMT"
    }
  ]
}
```

#### Get user info

**JWT authentication required**

Return info for the authenticated user. It fails with HTTP 403 if JWT token payload 'sub' does not equal user_id.

GET http://localhost:5000/api/users/<user_id>


*Response JSON:*
```
{
  "email": "mail@test.it",
  "name": "John",
  "registered_on": "Sun, 23 Apr 2017 16:39:35 GMT"
}
```

### Scrape API

#### Get images from url

**JWT authentication required**

Return images links for the requested url

GET http://localhost:5000/api/scrape/images?url=http://site.com/page.html


*Response JSON:*
```
{
  "images": [
    "/static/svg/evonove-logo-white.73ecd651e4e7.svg",
    "https://s3.eu-central-1.amazonaws.com/evonove.it/images/evonove-code-motion-software-developers.original.jpg",
    "https://s3.eu-central-1.amazonaws.com/evonove.it/images/Jewels_web3.original.jpg",
    "https://s3.eu-central-1.amazonaws.com/evonove.it/images/DSC_7419.original.jpg",
    "https://s3.eu-central-1.amazonaws.com/evonove.it/images/DSC_0065.original.jpg",
    "https://s3.eu-central-1.amazonaws.com/evonove.it/images/IMG_20161202_124549foo.original.png",
    "/static/svg/right-arrow.ae524b725a02.svg",
    "/static/svg/right-double-arrows.6a091c9e0394.svg",
    "/static/svg/down-arrow.53e7cc0a1c29.svg"
  ]
}
```

### Errors

#### code=401 - Missing JWT token
The header ```Authorization: Bearer <jwt_token>``` is missing

#### code=400 - Invalid JWT token
Invalid JWT Token. Need to reauth.

#### code=401 - Expired JWT token
JWT token has expired. Need to reauth.

#### code=400 - Cannot read payload
Input JSON payload is corrupted or malformed.

#### code=400 - Missing Parameter
Missing parameter like email or password is missing.

#### code=409 - Email already registered. Please sign in
An existing email was used during signup.

#### code=404 - User non existing. Please signup
Email used for login is not existing.

#### code=403 - Forbidden: Auth token is not valid for this resource
Access to a private resource. You can access to this resource only for entities owned by the authenticated user.

#### code=400 - Missing url parameter
Url was missing in /api/scrape/images.