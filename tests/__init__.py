import json

from flask_testing import TestCase

from server.config import FLASK

from app_bootstrap import app, db
from server.routes import user_services, scraping_services

# we register routes for testing
app.register_blueprint(user_services.blueprint(), url_prefix="/api/users")
app.register_blueprint(scraping_services.blueprint(), url_prefix="/api/scrape")


class TestMixin:
    def api_get(self, path, jwt=None):
        """
        Helper method for client get requests
        :param path: str, endpoint /api/users/
        :param jwt: str XXXXX.YYYYY.ZZZZZZ 
        """
        with self.client as client:
            headers = None
            if jwt:
                auth_header = 'Bearer {}'.format(jwt)
                headers = {'Authorization': auth_header}
            return client.get(path, headers=headers, content_type='application/json')

    def register_user(self, email, password, name=''):
        with self.client as client:
            return client.post(
                '/api/users/',
                data=json.dumps(dict(
                    email=email,
                    password=password,
                    name=name
                )),
                content_type='application/json',
            )

    def login_user(self, email, password):
        with self.client as client:
            return client.post(
                '/api/users/signin/',
                data=json.dumps(dict(
                    email=email,
                    password=password
                )),
                content_type='application/json',
            )


class BaseTestCase(TestCase, TestMixin):
    """ Base TestCase class """

    def create_app(self):
        app.config.update(FLASK['TEST'])
        app.config['TESTING'] = True
        return app

    def setUp(self):
        db.create_all()
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
