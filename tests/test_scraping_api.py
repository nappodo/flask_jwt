import os
import unittest

import requests_mock

from tests import BaseTestCase


BASEDIR = os.path.abspath(os.path.dirname(__file__))
SCRAPED_CONTENT_FOR_TEST = os.path.join(BASEDIR, 'data/response_content.html')


class TestScrapingEndpoints(BaseTestCase):

    def test_unauthorized_scrape(self):
        res = self.api_get('/api/scrape/images?url=https://evonove.it/blog/')
        self.assertEqual(res.status_code, 401)

    def test_missing_url(self):
        res = self.register_user('mail@test.com', 'password', 'John')
        auth_token = res.json['auth_token']
        res = self.api_get('/api/scrape/images', jwt=auth_token)
        self.assertEqual(res.status_code, 400)
        self.assertEqual(res.json['error'], 'Missing url parameter')

    def test_scrape_images(self):
        res = self.register_user('mail@test.com', 'password', 'John')
        auth_token = res.json['auth_token']
        with requests_mock.Mocker() as m:
            url_mock = 'https://evonove.it/blog/'
            with open(SCRAPED_CONTENT_FOR_TEST) as f:
                mock_content = f.read()
            m.get(url_mock, text=mock_content)
            res = self.api_get('/api/scrape/images?url=https://evonove.it/blog/', jwt=auth_token)
        images = res.json['images']
        self.assertTrue(len(images) == 9)


if __name__ == '__main__':
    unittest.main()

