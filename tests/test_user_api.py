import unittest

from server.models import User
from tests import BaseTestCase


class TestUserEndpoints(BaseTestCase):

    def test_signup_new_user(self):
        res = self.register_user('mail@test.com', 'password')
        self.assertEqual(res.status_code, 201)
        self.assertTrue('auth_token' in res.json)

    def test_signup_existing_user(self):
        self.register_user('mail@test.com', 'password')
        res = self.register_user('mail@test.com', 'password')
        self.assertEqual(res.status_code, 409)

    def test_signin_correct_credentials(self):
        self.register_user('mail@test.com', 'password')
        res = self.login_user('mail@test.com', 'password')
        self.assertEqual(res.status_code, 200)
        self.assertTrue('auth_token' in res.json)

    def test_signin_incorrect_credentials(self):
        self.register_user('mail@test.com', 'password')
        res = self.login_user('mail@test.com', 'password1')
        self.assertEqual(res.status_code, 401)

    def test_protected_wo_jwt(self):
        res = self.api_get('/api/users/')
        self.assertEqual(res.status_code, 401)

    def test_protected_with_jwt(self):
        self.register_user('test@test.com', 'password', 'Janet')
        res = self.register_user('mail@test.com', 'password', 'John')
        auth_token = res.json['auth_token']
        res = self.api_get('/api/users/', jwt=auth_token)
        self.assertEqual(res.status_code, 200)
        # todo other assert email
        users = {user['name'] for user in res.json['users']}
        self.assertEqual(len(users), 2)
        self.assertSetEqual(users, {'John', 'Janet'})

    def test_get_user(self):
        user = User.create(email='test@test.com', password='test', name='John')
        auth_token = user.generate_auth_token(user.id)
        res = self.api_get('/api/users/{}'.format(user.id), jwt=auth_token.decode())
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.json['email'], 'test@test.com')
        self.assertEqual(res.json['name'], 'John')

    def test_forbidden(self):
        user = User.create(email='test@test.com', password='test')
        other_user = User.create(email='test1@test.com', password='test')
        auth_token = user.generate_auth_token(user.id)
        res = self.api_get('/api/users/{}'.format(other_user.id), jwt=auth_token.decode())
        self.assertEqual(res.status_code, 403)


if __name__ == '__main__':
    unittest.main()

