import unittest

from app_bootstrap import db
from server.models import User
from tests import BaseTestCase


class TestUserModel(BaseTestCase):

    def test_encode_auth_token(self):
        user = User.create(email='test@test.com', password='test')
        auth_token = user.generate_auth_token(user.id)
        self.assertTrue(isinstance(auth_token, bytes))
        self.assertTrue(user.decode_auth_token(auth_token) == user.id)

    def test_decode_auth_token(self):
        user = User.create(email='test_another_user@test.com', password='test')
        auth_token = user.generate_auth_token(user.id)
        self.assertTrue(user.decode_auth_token(auth_token) == user.id)


if __name__ == '__main__':
    unittest.main()
