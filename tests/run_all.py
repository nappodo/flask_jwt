import glob
import os
import unittest

BASEDIR = os.path.abspath(os.path.dirname(__file__))
TESTDIR = os.path.join('tests')

test_files = glob.glob('{}/test_*.py'.format(TESTDIR))
print('Executing tests', test_files, sep='\n')
module_strings = [test_file[6:len(test_file)-3] for test_file in test_files]
suites = [unittest.defaultTestLoader.loadTestsFromName(test_file) for test_file in module_strings]
test_suite = unittest.TestSuite(suites)
test_runner = unittest.TextTestRunner().run(test_suite)
