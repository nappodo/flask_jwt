from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from server import config

app = Flask(__name__)
configuration_mode = config.SERVER_CONFIG_MODE
app.config.update(config.FLASK[configuration_mode])

db = SQLAlchemy(app)


@app.route('/')
def root():
    res = {
        "message": "Root!",
        "app.debug": app.debug,
    }

    return jsonify(res)
