from app_bootstrap import app, db
# from server.models import User
from server.routes import user_services, scraping_services


def create_tables():
    # Create table for each model if it does not exist.
    db.create_all()


def create_routes():
    # register routes
    app.register_blueprint(user_services.blueprint(), url_prefix="/api/users")
    app.register_blueprint(scraping_services.blueprint(), url_prefix="/api/scrape")

if __name__ == '__main__':
    create_tables()
    create_routes()
    app.run()
