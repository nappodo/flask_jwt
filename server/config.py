import os

# it can be DEV, TEST or PROD
# if ENV variables are not set config defaults to DEV
SERVER_CONFIG_MODE = os.getenv('CONFIGURATION_MODE', 'DEV')

DB_NAME = os.getenv('DB_NAME', 'jwt_evonove')
DB_USER = os.getenv('DB_USER', 'postgres')
DB_PASS = os.getenv('DB_PASS', '')
DB_IP = os.getenv('DB_IP', 'localhost')
SECRET = os.getenv('SECRET_KEY', os.urandom(24))

BASEDIR = os.path.abspath(os.path.dirname(__file__))
# on dev, db is a sqlite file
FLASK = {'DEV':
         {'SQLALCHEMY_DATABASE_URI': 'sqlite:///{}.sqlite'.format(os.path.join(BASEDIR, DB_NAME)),
          'SQLALCHEMY_COMMIT_ON_TEARDOWN': True,
          'SQLALCHEMY_TRACK_MODIFICATIONS': False,
          'DEBUG': True,
          'SECRET_KEY': SECRET,
          },
         }

# PostgreSQL instance for tests and in PROD
FLASK['TEST'] = FLASK['DEV'].copy()
FLASK['TEST']['SQLALCHEMY_DATABASE_URI'] = 'postgresql://{}:{}@{}/{}_test'.format(DB_USER, DB_PASS, DB_IP, DB_NAME)

FLASK['PROD'] = FLASK['TEST'].copy()
FLASK['PROD']['DEBUG'] = False
FLASK['PROD']['SQLALCHEMY_DATABASE_URI'] = 'postgresql://{}:{}@{}/{}'.format(DB_USER, DB_PASS, DB_IP, DB_NAME)
