import datetime

from passlib.apps import custom_app_context as pwd_context

from app_bootstrap import db
from server import utils


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(60), index=True, unique=True)
    name = db.Column(db.String(150))
    password_hash = db.Column(db.String(128))
    registered_on = db.Column(db.DateTime, nullable=False)

    def __init__(self, email, password, name):
        self.email = email
        self.registered_on = datetime.datetime.now()
        self.password_hash = pwd_context.hash(password)
        self.name = name

    def verify_password(self, password):
        """
        Verify password. Returns boolean
        :param password: plain password to verify
        :return: bool
        """
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expires=10):
        """
        Generate a JWT Token for user. Default expire is 10 minutes
        :return: bytes representing the JWT token
        :raise: JWT encoding exceptions
        """
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=expires),
            'iat': datetime.datetime.utcnow(),
            'sub': self.id
        }
        return utils.jwt_token(payload)

    @classmethod
    def decode_auth_token(cls, auth_token):
        """
        Decode the auth token
        :param auth_token:
        :return: user id
        :raise jwt.ExpiredSignatureError, jwt.InvalidTokenError
        """
        payload = utils.jwt_decode(auth_token)
        return payload['sub']

    @classmethod
    def create(cls, email, password, name=''):
        """
        Builder method to create a User instance and to save directly into DB
        :param email: user email
        :param password: password
        :param name: user full name
        :return: User instance
        """
        user = User(email=email, password=password, name=name)
        db.session.add(user)
        db.session.commit()
        return user
