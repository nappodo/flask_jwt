from functools import wraps

from flask import request
from jwt import DecodeError, ExpiredSignature

from server import utils


def jwt_required(view):
    @wraps(view)
    def decorated(*args, **kwargs):

        auth_token = utils.get_auth_token_from_headers(request)
        if not auth_token:
            return utils.error_response(401, 'Missing JWT token')

        try:
            _ = utils.jwt_decode(auth_token)
        except DecodeError:
            return utils.error_response(400, 'Invalid JWT token')
        except ExpiredSignature:
            return utils.error_response(401, 'Expired JWT token')

        return view(*args, **kwargs)

    return decorated

