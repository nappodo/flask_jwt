from flask import Blueprint, request

from server.decorators import jwt_required
from server.models import User
from server import utils


def blueprint():

    bp = Blueprint('user_services', __name__)

    def _check_input_data(post_data):
        """
        Helper method for pre checks for signup/signin
        :return: status_code, message tuple in case of error otherwise (0, '') for success
        """
        if not post_data:
            return 400, 'Cannot read payload'
        email = post_data.get('email')
        password = post_data.get('password')
        if not all((email, password)):
            return 400, 'Missing parameter'
        return 0, ''

    @bp.route("/", methods=['POST'])
    def signup():
        """
        Create a new user
        :return: JWT token to use for authenticated calls
        """
        post_data = utils.get_json_from_request(request)
        check_result = _check_input_data(post_data)
        if check_result[0]:
            return utils.error_response(check_result[0], check_result[1])

        email = post_data['email']
        password = post_data['password']
        user = User.query.filter_by(email=email).first()
        if user:
            # 409: Conflict
            return utils.error_response(409, 'Email already registered. Please sign in')

        name = post_data.get('name')
        user = User.create(email=email, password=password, name=name)
        jwt_token = user.generate_auth_token()
        # 201: Resource created
        return utils.response(201, auth_token=jwt_token.decode(), message='Successfully Registered')

    @bp.route("/signin/", methods=['POST'])
    def signin():
        """
        Login an existing user
        :return: JWT token to use for authenticated calls
        """
        post_data = utils.get_json_from_request(request)
        check_result = _check_input_data(post_data)
        if check_result[0]:
            return utils.error_response(check_result[0], check_result[1])

        email = post_data['email']
        password = post_data['password']

        user = User.query.filter_by(email=email).first()
        if not user:
            return utils.error_response(404, 'User non existing. Please signup')
        if not user.verify_password(password):
            # 401: Unauthorized
            return utils.error_response(401, 'Email already registered. Please sign in')

        jwt_token = user.generate_auth_token()
        return utils.response(200, auth_token=jwt_token.decode(), message='Successfully Logged in')

    @bp.route("/<int:user_id>", methods=['GET'])
    @jwt_required
    def user_get(user_id):

        user = User.query.get(user_id)
        if not user:
            return utils.error_response(404, 'Non existing')

        auth_token = utils.get_auth_token_from_headers(request)
        if not user_id == user.decode_auth_token(auth_token):
            return utils.error_response(403, 'Forbidden: Auth token is not valid for this resource')

        res = {'name': user.name, 'email': user.email, 'registered_on': user.registered_on}
        return utils.response(200, **res)

    @bp.route("/", methods=['GET'])
    @jwt_required
    def users_get():
        users = User.query.all()
        res = {'users': [{'name': u.name, 'registered_on': u.registered_on} for u in users]}
        return utils.response(200, **res)

    return bp
