from flask import Blueprint, request

from server import utils
from server.decorators import jwt_required
from server import scraping
from server.scraping import ScrapingException


def blueprint():

    bp = Blueprint('scraping_services', __name__)

    @bp.route("/images", methods=['GET'])
    @jwt_required
    def get_images():
        url = request.args.get('url')
        if not url:
            return utils.error_response(400, 'Missing url parameter')
        try:
            images = scraping.get_images(url)
        except ScrapingException as e:
            return utils.error_response(400, e.args[0])
        else:
            res = {'images': images}
            return utils.response(200, **res)

    return bp
