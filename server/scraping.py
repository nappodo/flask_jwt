import requests
from requests import RequestException
from bs4 import BeautifulSoup


class ScrapingException(Exception):
    pass


def get_images(url):
    """
    Find all images at a given url
    This version finds only images inside img tags.
    :param url: url to fetch  
    :return: list of images urls
    """
    try:
        res = requests.get(url)
    except RequestException as e:
        raise ScrapingException('Cannot query url {} - {}'.format(url, e))
    if not res or not res.status_code == 200:
        raise ScrapingException('Cannot query url {} - {}'.format(url, res))
    soup = BeautifulSoup(res.text, 'html.parser')
    image_tags = soup.find_all('img')
    return [img['src'] for img in image_tags]
